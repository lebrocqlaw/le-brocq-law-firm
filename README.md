Stephen Kenji Le Brocq is a client-driven attorney. Strong, efficient and he won’t back down! Mr. Le Brocq is an attorney licensed in Texas and Illinois, along with federal district and appellate courts, whose hard work, and dedication has resulted in exceptional results from his clients.

Address: 2150 N Josey Ln, Carrollton, TX 75006, USA

Phone: 469-930-4385
